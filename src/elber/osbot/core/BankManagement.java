package elber.osbot.core;

import java.util.ArrayList;
import java.util.HashMap;
import org.osbot.rs07.api.Bank;
import org.osbot.rs07.script.MethodProvider;

// Es recomendable usar esta clase cuando se esta cerca de un banco,
// de lo contrario puede generar comportamientos extraños
public class BankManagement {
    private MethodProvider methodProvider = null;
    
    public BankManagement() {
    }
    
    public BankManagement(final MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(final MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    public void get(int itemId, int quantity) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        bank.enableMode(Bank.BankMode.WITHDRAW_ITEM);
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        bank.withdraw(itemId, quantity);
        bank.close();
        MethodProvider.sleep(1500);
    }
    
    public void get(int itemId, int quantity, boolean withdrawNote) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        
        if(withdrawNote){
            bank.enableMode(Bank.BankMode.WITHDRAW_NOTE);
        }
        else{
            bank.enableMode(Bank.BankMode.WITHDRAW_ITEM);
        }
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        bank.withdraw(itemId, quantity);
        bank.close();
        MethodProvider.sleep(1500);
    }
    
    public void getAll(int itemId) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        bank.enableMode(Bank.BankMode.WITHDRAW_ITEM);
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        bank.withdrawAll(itemId);
        bank.close();
        MethodProvider.sleep(1500);
    }
    
    public void getAll(int itemId, boolean withdrawNote) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        
        if(withdrawNote){
            bank.enableMode(Bank.BankMode.WITHDRAW_NOTE);
        }
        else{
            bank.enableMode(Bank.BankMode.WITHDRAW_ITEM);
        }
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        bank.withdrawAll(itemId);
        bank.close();
        MethodProvider.sleep(1500);
    }
        
    public void saveAll() throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        bank.depositAll();
        bank.close();
        MethodProvider.sleep(1500);
    }
    
    public void saveAll(final int itemId) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        bank.depositAll(itemId);
        bank.close();
        MethodProvider.sleep(1500);
    }
    
    public boolean exist(final int itemId) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        
        bank.open();
        boolean result = false;
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        result = bank.contains(itemId);
        bank.close();
        MethodProvider.sleep(1500);
        
        return result;
    }
    
    public long stock(final int itemId) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        long _stock_ = 0L;
        
        bank.open();
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        if(bank.contains(itemId)){
            _stock_ = bank.getAmount(itemId);
            bank.close();
            MethodProvider.sleep(1500);
            return _stock_;
        }
        
        bank.close();
        MethodProvider.sleep(1500);
        
        return _stock_;
    }
    
    // retorna un HashMap con el siguiente formato
    // {
    //      itemId : cantidad_de_existencias
    //      itemId : cantidad_de_existencias
    // }
    public HashMap stock(final ArrayList<Integer> itemsIds) throws InterruptedException{
        Bank bank = this.methodProvider.getBank();
        HashMap<Integer, Long> result = new HashMap<>();
        
        bank.open();
        
        while(!bank.isOpen()){
            MethodProvider.sleep(1000);
        }
        
        itemsIds.forEach((itemId) -> {
            if(bank.contains(itemId)){
                result.put(itemId, bank.getAmount(itemId));
            }
            else{
                result.put(itemId, 0L);
            }
        });
        
        bank.close();
        MethodProvider.sleep(1500);
        
        return result;
    }
}
