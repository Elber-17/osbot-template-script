
package elber.osbot.core;

import org.osbot.rs07.api.map.Area;

public class Areas {
    final public static Area LUMBRIDGE_GOBLIN_VILLAGE = new Area(3239, 3254, 3266, 3220);
    final public static Area LUMBRIDGE_DAIRY_COW = new Area(3240, 3299, 3265, 3255);
    
    public Areas() {
    }
}
