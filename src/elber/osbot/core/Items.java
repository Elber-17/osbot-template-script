package elber.osbot.core;

public class Items {
    public static final int BONE = 526;
    public static final int BONE_NOTE = BONE + 1;
    public static final int COWHIDE = 1739;
    public static final int COWHIDE_NOTE = COWHIDE + 1;
    public static final int ENERGY_POTION_1 = 3014;
    public static final int ENERGY_POTION_1_NOTE = ENERGY_POTION_1 + 1;
    public static final int ENERGY_POTION_2 = 3012;
    public static final int ENERGY_POTION_2_NOTE = ENERGY_POTION_2 + 1;
    public static final int ENERGY_POTION_3 = 3010;
    public static final int ENERGY_POTION_3_NOTE = ENERGY_POTION_3 + 1;
    public static final int ENERGY_POTION_4 = 3008;
    public static final int ENERGY_POTION_4_NOTE = ENERGY_POTION_4 + 1;
    public static final int COINS = 995;
    
    public Items() {
    }
}
