package elber.osbot.core;

import java.util.ArrayList;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.script.MethodProvider;

public class MoneyMaker {
    private MethodProvider methodProvider = null;

    public MoneyMaker() {
    }
    
    public MoneyMaker(MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    public void sellBones(int price, int quantity) throws InterruptedException{
        BankManagement bankManagement = new BankManagement(this.methodProvider);
        Walk walk = new Walk(this.methodProvider);
        Sites sites = new Sites();
        TradeInGrandExchange tradeInGrandExchange = new TradeInGrandExchange(this.methodProvider);
        
        if(bankManagement.stock(Items.BONE) < quantity){
            this.collectBones(quantity);
        }
        
        walk.goTo(sites.GRAND_EXCHANGE);
        tradeInGrandExchange.sell(Items.BONE, price, (int) bankManagement.stock(Items.BONE));
    }
    
    private void collectBones(int quantity) throws InterruptedException{
        Walk walk = new Walk(this.methodProvider);
        Sites sites = new Sites();
        EnergyManagement energyManagement = new EnergyManagement(this.methodProvider);
        GroundItemSeeker groundItemSeeker = new GroundItemSeeker(this.methodProvider);
        GroundItemCollector groundItemCollector = new GroundItemCollector(this.methodProvider);
        InventoryManagement inventoryManagement = new InventoryManagement(this.methodProvider);
        BankManagement bankManagement = new BankManagement(this.methodProvider);
        ArrayList<Position> goodSites = new ArrayList<>();
        int energy_status = 0;
        
        goodSites.add(new Position(3256, 3246, 0));
        goodSites.add(new Position(3250, 3233, 0));
        goodSites.add(new Position(3253, 3225, 0));
        
        walk.goTo(sites.LUMBRIDGE_GOBLIN_VILLAGE);
        
        while(true){
            energyManagement.manage();
            energy_status = energyManagement.getStatus();
            GroundItem bone = groundItemSeeker.search(Areas.LUMBRIDGE_GOBLIN_VILLAGE, Items.BONE);
            
            if(bone == null){
                for(Position goodSite : goodSites){
                    walk.goTo(goodSite);
                    bone = groundItemSeeker.search(Areas.LUMBRIDGE_GOBLIN_VILLAGE, Items.BONE);
                    
                    if(bone != null){
                        break;
                    }
                }
                
                if(bone == null){
                    continue;
                }
            }
            
            if(energy_status == energyManagement.USABLE){
                groundItemCollector.collect(bone, true);
            }
            else{
                groundItemCollector.collect(bone);
            }
            
            
            if(inventoryManagement.isInventoryFull()){
                walk.goTo(sites.LUMBRIDGE_BANK_UPPER);
                bankManagement.saveAll();
                
                if(bankManagement.stock(Items.BONE) >= quantity){
                    break;
                }
                
                walk.goTo(sites.LUMBRIDGE_GOBLIN_VILLAGE);
            }
        }
    }
}
