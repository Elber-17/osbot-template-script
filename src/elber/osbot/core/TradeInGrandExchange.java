package elber.osbot.core;

import org.osbot.rs07.api.GrandExchange;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.event.InteractionEvent;
import org.osbot.rs07.script.MethodProvider;

public class TradeInGrandExchange {
    MethodProvider methodProvider = null;
    
    public TradeInGrandExchange() {
    }
    
    public TradeInGrandExchange(MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    public boolean buy(int itemId, String searchTerm, int price, int quantity, boolean saveInTheBank) throws InterruptedException{
        GrandExchange grandExchange = this.methodProvider.getGrandExchange();
        long moneyBalance = this.getMoneyBalance();
        BankManagement bankManagement = new BankManagement(this.methodProvider);
        
        if( (price * quantity) > moneyBalance ){
            return false;
        }
        
        this.takeMoney();
        this.openGrandExchange();
        grandExchange.buyItem(itemId, searchTerm, price, quantity);
        MethodProvider.sleep(2000);
        
        while(
                grandExchange.getStatus(GrandExchange.Box.BOX_1) == GrandExchange.Status.PENDING_BUY ||
                grandExchange.getStatus(GrandExchange.Box.BOX_1) == GrandExchange.Status.COMPLETING_BUY
             ){
            MethodProvider.sleep(2000);
        }
        
        grandExchange.collect();
        this.closeGrandExchange();
        
        if(saveInTheBank){
            bankManagement.saveAll();
        }
        
        return true;
    }
    
    public boolean sell(int itemId, int price, int quantity) throws InterruptedException{
        GrandExchange grandExchange = this.methodProvider.getGrandExchange();
        BankManagement bankManagement = new BankManagement(this.methodProvider);
        
        bankManagement.getAll(itemId, true);
        this.openGrandExchange();
        grandExchange.sellItem(itemId + 1, price, quantity);
        MethodProvider.sleep(2000);
        
        while(
                grandExchange.getStatus(GrandExchange.Box.BOX_1) == GrandExchange.Status.PENDING_SALE ||
                grandExchange.getStatus(GrandExchange.Box.BOX_1) == GrandExchange.Status.COMPLETING_SALE
             ){
            MethodProvider.sleep(2000);
        }
        
        grandExchange.collect();
        this.closeGrandExchange();
        bankManagement.saveAll();
        
        return true;
    }
    
    private long getMoneyBalance() throws InterruptedException{
        BankManagement bankManagement = new BankManagement(this.methodProvider);
        
        return bankManagement.stock(Items.COINS);
    }
    
    private void takeMoney() throws InterruptedException{
        BankManagement bankManagement = new BankManagement(this.methodProvider);
        
        bankManagement.getAll(Items.COINS);
    }
    
    private void openGrandExchange() throws InterruptedException{
        Entity grandExchangeClerk = this.methodProvider.getNpcs().closest(2148);
        CameraManagement cameraManagement = new CameraManagement(this.methodProvider);
        InteractionEvent interactionEvent = new InteractionEvent(grandExchangeClerk, "Exchange");
        interactionEvent.setOperateCamera(false);
        
        this.methodProvider.execute(interactionEvent);
        
        while(!this.methodProvider.getGrandExchange().isOpen()){
            MethodProvider.sleep(1500);
        }
        
        cameraManagement.normalize();
    }

    private void closeGrandExchange() throws InterruptedException{
        CameraManagement cameraManagement = new CameraManagement(this.methodProvider);
        GrandExchange grandExchange = this.methodProvider.getGrandExchange();
        
        grandExchange.close();
        
        while(grandExchange.isOpen()){
            MethodProvider.sleep(1500);
        }
        
        cameraManagement.normalize();
    }
    
    
}
