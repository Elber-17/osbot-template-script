
package elber.osbot.core;

import java.awt.Graphics2D;
import org.osbot.rs07.script.Script;
import org.osbot.rs07.script.ScriptManifest;

@ScriptManifest(author = "your name", info = "info", name = "script name", version = 0, logo = "")
public class TemplateScript extends Script{
    
    @Override
    public void onStart() throws InterruptedException{
        
    }
    
    @Override
    public void onPaint(Graphics2D g){
        
    }
    
    @Override
    public int onLoop() throws InterruptedException{
        
        
        return 1000; // miliseconds to execute next loop
    }
    
    @Override 
    public void onExit() throws InterruptedException{
        
    }
}
