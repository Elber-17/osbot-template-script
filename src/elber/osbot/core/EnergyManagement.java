package elber.osbot.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;
import org.osbot.rs07.script.MethodProvider;

public class EnergyManagement {
    private MethodProvider methodMethodProvider = null;
    public final int CHARGING = 1;
    public final int USABLE = 2;
    private int status = CHARGING;

    public EnergyManagement() {
    }
    
    public EnergyManagement(MethodProvider _methodProvider_) {
        this.methodMethodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(MethodProvider _methodProvider_){
        this.methodMethodProvider = _methodProvider_;
    }

    public int getStatus() {
        return status;
    }
    
    public void manage(){
        if(this.methodMethodProvider.getSettings().getRunEnergy() == 100){
            this.status = this.USABLE;
        }
        else if(this.methodMethodProvider.getSettings().getRunEnergy() < 10){
            this.status = this.CHARGING;
        }
    }
    
    public boolean reloadEnergy() throws InterruptedException{
        int energy = this.methodMethodProvider.getSettings().getRunEnergy();
        BankManagement bankManagement = new BankManagement(this.methodMethodProvider);
        ArrayList<Integer> potions = new ArrayList<>();
        HashMap<Integer, Integer> potionsAvailable = new HashMap<>();
        
        potions.add(Items.ENERGY_POTION_1);
        potions.add(Items.ENERGY_POTION_2);
        potions.add(Items.ENERGY_POTION_3);
        potions.add(Items.ENERGY_POTION_4);
        
        HashMap<Integer, Long> stockPotions = bankManagement.stock(potions);
        int energyThatCanBeReloaded = this.getEnergyThatCanBeReloaded(potions, stockPotions);
        
        if(energy == 100){
            this.status = this.USABLE;
            return true;
        }
        
        if(energyThatCanBeReloaded >= (100 - energy)){
            this.reload(stockPotions, energy);
            return true;
        }
        
        stockPotions.entrySet().stream().filter((stockPotionsEntry) -> !(stockPotionsEntry.getValue() == 0)).forEachOrdered((stockPotionsEntry) -> {
            potionsAvailable.put(stockPotionsEntry.getKey(), stockPotionsEntry.getValue().intValue());
        });
        
        this.reload(potionsAvailable);
        
        return false;
    }
    
    private int getEnergyThatCanBeReloaded(ArrayList<Integer> potions, HashMap<Integer, Long> stockPotions) throws InterruptedException{
        int energyThatCanBeReloaded = 0;
        int energyRechargePerPotion = 10;
        
        Collections.sort(potions);
        Collections.reverse(potions);
        
        for(Integer potion : potions){
            energyThatCanBeReloaded += stockPotions.get(potion) * energyRechargePerPotion;
            
            if(energyThatCanBeReloaded >= 100){
                return energyThatCanBeReloaded;
            }
            
            energyRechargePerPotion += 10;
        }
        
        return energyThatCanBeReloaded;
    }
    
    private void reload(HashMap<Integer, Long> stockPotions, int energy) throws InterruptedException{
        HashMap<Integer, Integer> potionsToUse = this.getPotionsToUse(stockPotions, energy);
        BankManagement bankManagement = new BankManagement(this.methodMethodProvider);
        
        for(HashMap.Entry<Integer, Integer> potionsToUseEntry : potionsToUse.entrySet()){
            int itemId = potionsToUseEntry.getKey();
            int quantity = potionsToUseEntry.getValue();
            
            bankManagement.get(itemId, quantity);
        }
        
        this.drinkEnergyPotions(potionsToUse);
    }
    
    private void reload(HashMap<Integer, Integer> potionsAvailable) throws InterruptedException{
        BankManagement bankManagement = new BankManagement(this.methodMethodProvider);
        
        for(HashMap.Entry<Integer, Integer> potionsToUseEntry : potionsAvailable.entrySet()){
            int itemId = potionsToUseEntry.getKey();
            int quantity = potionsToUseEntry.getValue();
            
            bankManagement.get(itemId, quantity);
        }
        
        this.drinkEnergyPotions(potionsAvailable);
    }
    
    // retorna un HashMap con el siguiente formato
    // {
    //      itemId : cantidad_que_se_necesita_para_recargar_energia
    //      itemId : cantidad_que_se_necesita_para_recargar_energia
    //      itemId : cantidad_que_se_necesita_para_recargar_energia
    //      itemId : cantidad_que_se_necesita_para_recargar_energia
    // }
    private HashMap<Integer, Integer> getPotionsToUse(HashMap<Integer, Long> stockPotions, int energy){
        int missingEnergy = 100 - energy;
        int rechargedEnergy = 0;
        int energyRechargePerPotion = 10;
        HashMap<Integer, Integer> potionsToBeUsed = new HashMap<>();
        SortedMap<Integer, Long> reverseOrderStockPotions = new TreeMap<>(Collections.reverseOrder());
        
        reverseOrderStockPotions.putAll(stockPotions);
        
        for(SortedMap.Entry<Integer, Long> reverseOrderStockPotionsEntry : reverseOrderStockPotions.entrySet()){
            int itemId = reverseOrderStockPotionsEntry.getKey();
            long stock = reverseOrderStockPotionsEntry.getValue();
            
            for(int stockToBeUsed = 1; stockToBeUsed <= stock; stockToBeUsed++){
                if(rechargedEnergy >= missingEnergy){
                    break;
                }
                
                if(potionsToBeUsed.containsKey(itemId)){
                    potionsToBeUsed.replace(itemId, stockToBeUsed);
                }
                else{
                    potionsToBeUsed.put(itemId, stockToBeUsed);
                }
                
                rechargedEnergy += energyRechargePerPotion;
            }
            
            energyRechargePerPotion += 10;
        }
        
        return potionsToBeUsed;
    }

    private void drinkEnergyPotions(HashMap<Integer, Integer> potionsToUse) throws InterruptedException {
        InventoryManagement inventoryManagement = new InventoryManagement(this.methodMethodProvider);
        
        if(potionsToUse.containsKey(Items.ENERGY_POTION_4)){
            for(int index = 0; index < potionsToUse.get(Items.ENERGY_POTION_4); index++){
                inventoryManagement.interact(Items.ENERGY_POTION_4, "Drink");
                
                if(this.methodMethodProvider.getSettings().getRunEnergy() >= 95){
                    this.saveLeftover();
                    return;
                }
                
                if(potionsToUse.containsKey(Items.ENERGY_POTION_3)){
                    int potion3NewStock = potionsToUse.get(Items.ENERGY_POTION_3) + 1;
                    potionsToUse.replace(Items.ENERGY_POTION_3, potion3NewStock);
                }
                else{
                    potionsToUse.put(Items.ENERGY_POTION_3, 1);
                }
            }
        }
        
        if(potionsToUse.containsKey(Items.ENERGY_POTION_3)){
            for(int index = 0; index < potionsToUse.get(Items.ENERGY_POTION_3); index++){
                inventoryManagement.interact(Items.ENERGY_POTION_3, "Drink");
                
                if(this.methodMethodProvider.getSettings().getRunEnergy() >= 95){
                    this.saveLeftover();
                    return;
                }
                
                if(potionsToUse.containsKey(Items.ENERGY_POTION_2)){
                    int potion2NewStock = potionsToUse.get(Items.ENERGY_POTION_2) + 1;
                    potionsToUse.replace(Items.ENERGY_POTION_2, potion2NewStock);
                }
                else{
                    potionsToUse.put(Items.ENERGY_POTION_2, 1);
                }
            }
        }
        
        if(potionsToUse.containsKey(Items.ENERGY_POTION_2)){
            for(int index = 0; index < potionsToUse.get(Items.ENERGY_POTION_2); index++){
                inventoryManagement.interact(Items.ENERGY_POTION_2, "Drink");
                
                if(this.methodMethodProvider.getSettings().getRunEnergy() >= 95){
                    this.saveLeftover();
                    return;
                }
                
                if(potionsToUse.containsKey(Items.ENERGY_POTION_1)){
                    int potion1NewStock = potionsToUse.get(Items.ENERGY_POTION_1) + 1;
                    potionsToUse.replace(Items.ENERGY_POTION_1, potion1NewStock);
                }
                else{
                    potionsToUse.put(Items.ENERGY_POTION_1, 1);
                }
            }
        }
        
        if(potionsToUse.containsKey(Items.ENERGY_POTION_1)){
            for(int index = 0; index < potionsToUse.get(Items.ENERGY_POTION_1); index++){
                inventoryManagement.interact(Items.ENERGY_POTION_1, "Drink");
                
                if(this.methodMethodProvider.getSettings().getRunEnergy() >= 95){
                    this.saveLeftover();
                    return;
                }
            }
        }
        
        this.saveLeftover();
    }

    private void saveLeftover() throws InterruptedException{
        BankManagement bankManagement = new BankManagement(this.methodMethodProvider);
        bankManagement.saveAll();
    }
}
