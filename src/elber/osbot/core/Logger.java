package elber.osbot.core;

import org.osbot.rs07.script.MethodProvider;

public class Logger {
    private MethodProvider methodProvider = null;
    private int counter = 0;

    public Logger() {
    }
    
    public Logger(final MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(final MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    
    public void log(final Object message){
        counter += 1;
        this.methodProvider.log("\n****************    " + counter + ")      " + message + "           ***************");
    }
    
    public void log(final String description, final Object message){
        counter += 1;
       this. methodProvider.log("\n****************    " + counter + ")      " + description + " : " +  message + "           ***************");
    }
}
