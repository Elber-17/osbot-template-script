package elber.osbot.core;

import org.osbot.rs07.api.Inventory;
import org.osbot.rs07.api.model.Item;
import org.osbot.rs07.script.MethodProvider;

public class InventoryManagement {
    private MethodProvider methodProvider = null;
    
    public InventoryManagement() {
    }
    
    public InventoryManagement(final MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(final MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    public int getEmptySlots(){
        Inventory inventory = this.methodProvider.getInventory();
        return inventory.getEmptySlots();
    }
    
    public boolean isInventoryFull(){
        return this.getEmptySlots() == 0;
    }
    
    public void interact(int itemId, String action) throws InterruptedException{
        Item item = this.methodProvider.getInventory().getItem(itemId);
        
        item.interact(action);
        MethodProvider.sleep(2000);
    }
    
    public void dropAllExcept(int itemId) throws InterruptedException{
        Inventory inventory = this.methodProvider.getInventory();
        
        inventory.dropAllExcept(itemId);
        MethodProvider.sleep(500);
    }
}
