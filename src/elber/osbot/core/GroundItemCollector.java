package elber.osbot.core;

import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.event.Event;
import org.osbot.rs07.event.InteractionEvent;
import org.osbot.rs07.script.MethodProvider;

public class GroundItemCollector {
    private MethodProvider methodProvider = null;
    private final CameraManagement cameraManagement = new CameraManagement();

    public GroundItemCollector() {
    }
    
    public GroundItemCollector(final MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
        this.cameraManagement.setMethoProvider(this.methodProvider);
    }
    
    public void setMethodProvider(final MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
        this.cameraManagement.setMethoProvider(this.methodProvider);
    }
    
    public void collect(final GroundItem groundItem) throws InterruptedException{
        InventoryManagement inventoryManagement = new InventoryManagement(this.methodProvider);
        Walk walk = new Walk(this.methodProvider);
        
        if(inventoryManagement.isInventoryFull()){
            return;
        }
        
        int emptySlots = inventoryManagement.getEmptySlots();
        InteractionEvent interactionEvent = new InteractionEvent(groundItem, "Take");
        interactionEvent.setOperateCamera(false);
        this.methodProvider.getSettings().setRunning(false);
        this.methodProvider.execute(interactionEvent);
        MethodProvider.sleep(700);
        
        if(!this.methodProvider.myPlayer().isMoving()){
            walk.goToFast(groundItem.getPosition());
        }
        
        while(emptySlots == inventoryManagement.getEmptySlots()){
            Entity door = this.methodProvider.getObjects().closest("Door");
            
            if(door != null){
                if(walk.isNearPosition(this.methodProvider.myPosition(), door.getPosition())){
                   if(door.hasAction("Open")){
                        InteractionEvent interactionEventDoor = new InteractionEvent(door, "Open");
                        interactionEventDoor.setOperateCamera(false);
                        this.methodProvider.execute(interactionEventDoor);
                    }
                }
                
            }
            
            if(!groundItem.exists()){
                break;
            }
            
            if(
                (interactionEvent.getStatus() == Event.EventStatus.FAILED ||
                interactionEvent.getStatus() == Event.EventStatus.FINISHED) &&
                groundItem.exists()
               ){
                interactionEvent = new InteractionEvent(groundItem, "Take");
                interactionEvent.setOperateCamera(false);
                this.methodProvider.getSettings().setRunning(false);
                this.methodProvider.execute(interactionEvent);
                MethodProvider.sleep(700);

                if(!this.methodProvider.myPlayer().isMoving()){
                    walk.goToFast(groundItem.getPosition());
                }
            }
            
            MethodProvider.sleep(100);
        }
        
        this.cameraManagement.normalize();
    }
    
    public void collect(final GroundItem groundItem, boolean running) throws InterruptedException{
        InventoryManagement inventoryManagement = new InventoryManagement(this.methodProvider);
        Walk walk = new Walk(this.methodProvider);
        
        if(inventoryManagement.isInventoryFull()){
            return;
        }
        
        int emptySlots = inventoryManagement.getEmptySlots();
        InteractionEvent interactionEvent = new InteractionEvent(groundItem, "Take");
        interactionEvent.setOperateCamera(false);
        this.methodProvider.getSettings().setRunning(running);
        this.methodProvider.execute(interactionEvent);
        MethodProvider.sleep(700);
        
        if(!this.methodProvider.myPlayer().isMoving()){
            walk.goToFast(groundItem.getPosition());
        }
        
        while(emptySlots == inventoryManagement.getEmptySlots()){
            Entity door = this.methodProvider.getObjects().closest("Door");
            
            if(door != null){
                if(walk.isNearPosition(this.methodProvider.myPosition(), door.getPosition())){
                   if(door.hasAction("Open")){
                        InteractionEvent interactionEventDoor = new InteractionEvent(door, "Open");
                        interactionEventDoor.setOperateCamera(false);
                        this.methodProvider.execute(interactionEventDoor);
                    }
                }
                
            }
            
            if(!groundItem.exists()){
                break;
            }
            
            if(
                (interactionEvent.getStatus() == Event.EventStatus.FAILED ||
                interactionEvent.getStatus() == Event.EventStatus.FINISHED) &&
                groundItem.exists()
               ){
                interactionEvent = new InteractionEvent(groundItem, "Take");
                interactionEvent.setOperateCamera(false);
                this.methodProvider.getSettings().setRunning(running);
                this.methodProvider.execute(interactionEvent);
                MethodProvider.sleep(700);

                if(!this.methodProvider.myPlayer().isMoving()){
                    walk.goToFast(groundItem.getPosition());
                }
            }
            
            MethodProvider.sleep(100);
        }
        
        this.cameraManagement.normalize();
    }
}
