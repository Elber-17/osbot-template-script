package elber.osbot.core;

import java.util.ArrayList;
import java.util.HashMap;
import org.osbot.rs07.api.map.Position;

public class Sites {
    public ArrayList<HashMap> LUMBRIDGE_BANK_UPPER = new ArrayList<>(); // PARA SUBIR AL BANCO
    public ArrayList<HashMap> LUMBRIDGE_BANK_LOWER = new ArrayList<>();// PARA BAJAR DEL BANCO
    final public Position LUMBRIDGE_GOBLIN_VILLAGE = new Position(3259, 3229,0);
    final public Position LUMBRIDGE_DAIRY_COW = new Position(3260, 3267,0);
    final public Position GRAND_EXCHANGE = new Position(3166, 3487, 0);
    final public Position EAST_VARROCK_BANK = new Position(3253, 3420, 0);
    final public Position WEST_VARROCK_BANK = new Position(3185, 3440, 0);
    final public Position LUMBRIDGE = new Position(3235, 3219, 0);

    public Sites() {
        HashMap<Object, Object> tempHashMap = new HashMap<>();
        tempHashMap.put("site", new Position(3206, 3228, 0));
        tempHashMap.put("floorsToClimb", 2);
        tempHashMap.put("floorsToGoDown", 0);
        this.LUMBRIDGE_BANK_UPPER.add((HashMap)tempHashMap.clone());
        tempHashMap.clear();
        tempHashMap.put("site", new Position(3209, 3220, 2));
        tempHashMap.put("floorsToClimb", 0);
        tempHashMap.put("floorsToGoDown", 0);
        this.LUMBRIDGE_BANK_UPPER.add((HashMap)tempHashMap.clone());
        tempHashMap.clear();
        tempHashMap.put("site", new Position(3206, 3228, 2));
        tempHashMap.put("floorsToClimb", 0);
        tempHashMap.put("floorsToGoDown", 2);
        this.LUMBRIDGE_BANK_LOWER.add((HashMap)tempHashMap.clone());
        tempHashMap.clear();
        tempHashMap.put("site", new Position(3206, 3228, 0));
        tempHashMap.put("floorsToClimb", 0);
        tempHashMap.put("floorsToGoDown", 0);
        this.LUMBRIDGE_BANK_LOWER.add((HashMap)tempHashMap.clone());
    }
}
