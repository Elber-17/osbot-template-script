package elber.osbot.core;

import org.osbot.rs07.api.Camera;
import org.osbot.rs07.script.MethodProvider;

public class CameraManagement {
    private MethodProvider methodProvider = null;
    
    public CameraManagement() {
    }
    
    public CameraManagement(MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethoProvider(MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    public void normalize(){
        Camera camera = this.methodProvider.getCamera();
        
        camera.moveYaw(0);
        camera.toTop();
    }
    
}
