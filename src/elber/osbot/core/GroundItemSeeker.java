package elber.osbot.core;

import org.osbot.rs07.api.map.Area;
import org.osbot.rs07.api.model.GroundItem;
import org.osbot.rs07.script.MethodProvider;

public class GroundItemSeeker {
    private MethodProvider methodProvider = null;
    
    public GroundItemSeeker() {
    }
    
    public GroundItemSeeker(MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
    }
    
    public void setMethodProvider(MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
    }
    
    public GroundItem search(int id){
        GroundItem groundItem = this.methodProvider.getGroundItems().closest(id);
        
        return groundItem;
    }
    
    public GroundItem search(Area area, int id){
        GroundItem groundItem = this.methodProvider.getGroundItems().closest(area, id);
        
        return groundItem;
    }
}
