package elber.osbot.core;

import java.util.ArrayList;
import java.util.HashMap;
import org.osbot.rs07.api.map.Position;
import org.osbot.rs07.api.map.constants.Banks;
import org.osbot.rs07.api.model.Entity;
import org.osbot.rs07.event.InteractionEvent;
import org.osbot.rs07.event.WebWalkEvent;
import org.osbot.rs07.script.MethodProvider;

public class Walk {
    private MethodProvider methodProvider = null;
    private final CameraManagement cameraManagement = new CameraManagement();
    final public int RUN = 10;
    final private int WALK = 101;
    
    public Walk() {
    }
    
    public Walk(final MethodProvider _methodProvider_) {
        this.methodProvider = _methodProvider_;
        this.cameraManagement.setMethoProvider(this.methodProvider);
    }
    
    public void setMethodProvider(final MethodProvider _methodProvider_){
        this.methodProvider = _methodProvider_;
        this.cameraManagement.setMethoProvider(this.methodProvider);
    }
    
    public boolean equalPositions(final Position position1, final Position position2){
        if(position1.getX() != position2.getX()){
            return false;
        }
        
        if(position1.getY() != position2.getY()){
            return false;
        }
        
        if(position1.getZ() != position2.getZ()){
            return false;
        }
        
        if(position1.getX() != position2.getX()){
            return false;
        }
        
        if(position1.getY() != position2.getY()){
            return false;
        }
        
        return position1.getZ() == position2.getZ();
    }
    
    public boolean isNearPosition(final Position entityPosition, final Position destinationPosition){
        int xDistance = entityPosition.getX() - destinationPosition.getX();
        int yDistance = entityPosition.getY() - destinationPosition.getY();
        int zDistance = entityPosition.getZ() - destinationPosition.getZ();
        
        return (xDistance >= -3 && xDistance <= 3) && (yDistance >= -3 && yDistance <= 3) && (zDistance >= -3 && zDistance <= 3);
    }
    
    public boolean goTo(final Position site) throws InterruptedException{
        WebWalkEvent webWalkEvent = null;
        Sites sites = new Sites();
        
        if(Banks.LUMBRIDGE_UPPER.contains(this.methodProvider.myPlayer())){
            this.goTo(sites.LUMBRIDGE_BANK_LOWER);
        }
        
        while(!this.equalPositions(this.methodProvider.myPosition(), site) && !this.isNearPosition(this.methodProvider.myPosition(), site)){
            webWalkEvent = new WebWalkEvent(site);
            webWalkEvent.setEnergyThreshold(this.WALK);
            webWalkEvent.setMoveCameraDuringWalking(false);
            this.methodProvider.getSettings().setRunning(false);
            this.methodProvider.execute(webWalkEvent);
            MethodProvider.sleep(1000);

            while(this.methodProvider.myPlayer().isMoving()){
                MethodProvider.sleep(2000);
            }
        }
        
        this.cameraManagement.normalize();
        
        return true;
    }
    public boolean goToFast(final Position site) throws InterruptedException{
        WebWalkEvent webWalkEvent = null;
        Sites sites = new Sites();
        
        if(Banks.LUMBRIDGE_UPPER.contains(this.methodProvider.myPlayer())){
            this.goTo(sites.LUMBRIDGE_BANK_LOWER);
        }
        
        while(!this.equalPositions(this.methodProvider.myPosition(), site) && !this.isNearPosition(this.methodProvider.myPosition(), site)){
            webWalkEvent = new WebWalkEvent(site);
            webWalkEvent.setEnergyThreshold(this.RUN);
            webWalkEvent.setMoveCameraDuringWalking(false);
            this.methodProvider.getSettings().setRunning(true);
            this.methodProvider.execute(webWalkEvent);
            MethodProvider.sleep(700);

            while(this.methodProvider.myPlayer().isMoving()){
                MethodProvider.sleep(100);
            }
        }
        
        this.cameraManagement.normalize();
        
        return true;
    }
    
    public boolean goTo(final ArrayList<HashMap> path) throws InterruptedException{
        Position site = null;
        int floorsToClimb = 0;
        int floorsToGoDown = 0;
        WebWalkEvent webWalkEvent = null;
        Entity stairs = null;
        InteractionEvent interactionEvent = null;
        
        for(HashMap info : path){
            site = (Position)info.get("site");
            floorsToClimb = (int)info.get("floorsToClimb");
            floorsToGoDown = (int)info.get("floorsToGoDown");
            
            while(!this.equalPositions(this.methodProvider.myPosition(), site) && !this.isNearPosition(this.methodProvider.myPosition(), site)){
                webWalkEvent = new WebWalkEvent(site);
                webWalkEvent.setEnergyThreshold(this.WALK);
                webWalkEvent.setMoveCameraDuringWalking(false);
                this.methodProvider.getSettings().setRunning(false);
                this.methodProvider.execute(webWalkEvent);
                MethodProvider.sleep(1000);

                while(this.methodProvider.myPlayer().isMoving()){
                    MethodProvider.sleep(2000);
                }
            }
            
            this.cameraManagement.normalize();
            
            if(floorsToClimb != 0){
                for(int level = 0; level < floorsToClimb; level++){
                    int beforeZIndex = this.methodProvider.myPlayer().getPosition().getZ();
                    stairs = this.methodProvider.getObjects().closest("Staircase");
                    interactionEvent = new InteractionEvent(stairs, "Climb-up");
                    interactionEvent.setOperateCamera(false);
                    this.methodProvider.execute(interactionEvent);
                    MethodProvider.sleep(1000);
                    
                    while(beforeZIndex == this.methodProvider.myPlayer().getPosition().getZ()){
                        interactionEvent = new InteractionEvent(stairs, "Climb-up");
                        interactionEvent.setOperateCamera(false);

                        // refactorizar
                        while(!this.isNearPosition(this.methodProvider.myPosition(), site)){
                            webWalkEvent = new WebWalkEvent(site);
                            webWalkEvent.setEnergyThreshold(this.WALK);
                            webWalkEvent.setMoveCameraDuringWalking(false);
                            this.methodProvider.getSettings().setRunning(false);
                            this.methodProvider.execute(webWalkEvent);
                            MethodProvider.sleep(1000);

                            while(this.methodProvider.myPlayer().isMoving()){
                                MethodProvider.sleep(2000);
                            }
                        }

                        this.cameraManagement.normalize();

                        this.methodProvider.execute(interactionEvent);
                        MethodProvider.sleep(1000);
                        
                    }
                }
            }
            else if(floorsToGoDown != 0){
                for(int level = 0; level < floorsToGoDown; level++){
                    int beforeZIndex = this.methodProvider.myPlayer().getPosition().getZ();
                    stairs = this.methodProvider.getObjects().closest("Staircase");
                    interactionEvent = new InteractionEvent(stairs, "Climb-down");
                    interactionEvent.setOperateCamera(false);
                    this.methodProvider.execute(interactionEvent);
                    MethodProvider.sleep(1000);
                    
                    while(beforeZIndex == this.methodProvider.myPlayer().getPosition().getZ()){
                        interactionEvent = new InteractionEvent(stairs, "Climb-down");
                        interactionEvent.setOperateCamera(false);

                         // refactorizar
                        while(!this.isNearPosition(this.methodProvider.myPosition(), site)){
                            webWalkEvent = new WebWalkEvent(site);
                            webWalkEvent.setEnergyThreshold(this.WALK);
                            webWalkEvent.setMoveCameraDuringWalking(false);
                            this.methodProvider.getSettings().setRunning(false);
                            this.methodProvider.execute(webWalkEvent);
                            MethodProvider.sleep(1000);

                            while(this.methodProvider.myPlayer().isMoving()){
                                MethodProvider.sleep(2000);
                            }
                        }

                        this.cameraManagement.normalize();

                        this.methodProvider.execute(interactionEvent);
                        MethodProvider.sleep(1000);
                    }
                }
            }
        }
        
        return true;
    }
    
    public boolean goTo(final Position site, int energyThreshold) throws InterruptedException{
        WebWalkEvent webWalkEvent = null;
        Sites sites = new Sites();
        
        if(Banks.LUMBRIDGE_UPPER.contains(this.methodProvider.myPlayer())){
            this.goTo(sites.LUMBRIDGE_BANK_LOWER, energyThreshold);
        }
        
        while(!this.equalPositions(this.methodProvider.myPosition(), site) && !this.isNearPosition(this.methodProvider.myPosition(), site)){
            webWalkEvent = new WebWalkEvent(site);
            webWalkEvent.setEnergyThreshold(energyThreshold);
            webWalkEvent.setMoveCameraDuringWalking(false);
            this.methodProvider.getSettings().setRunning(true);
            this.methodProvider.execute(webWalkEvent);
            MethodProvider.sleep(1000);

            while(this.methodProvider.myPlayer().isMoving()){
                MethodProvider.sleep(2000);
            }
        }
        
        this.cameraManagement.normalize();
        
        return true;
    }
    
    public boolean goTo(final ArrayList<HashMap> path, int energyThreshold) throws InterruptedException{
        Position site = null;
        int floorsToClimb = 0;
        int floorsToGoDown = 0;
        WebWalkEvent webWalkEvent = null;
        Entity stairs = null;
        InteractionEvent interactionEvent = null;
        
        for(HashMap info : path){
            site = (Position)info.get("site");
            floorsToClimb = (int)info.get("floorsToClimb");
            floorsToGoDown = (int)info.get("floorsToGoDown");
            
            while(!this.equalPositions(this.methodProvider.myPosition(), site) && !this.isNearPosition(this.methodProvider.myPosition(), site)){
                webWalkEvent = new WebWalkEvent(site);
                webWalkEvent.setEnergyThreshold(energyThreshold);
                webWalkEvent.setMoveCameraDuringWalking(false);
                this.methodProvider.getSettings().setRunning(true);
                this.methodProvider.execute(webWalkEvent);
                MethodProvider.sleep(1000);

                while(this.methodProvider.myPlayer().isMoving()){
                    MethodProvider.sleep(2000);
                }
            }
            
            this.cameraManagement.normalize();
            
            if(floorsToClimb != 0){
                for(int level = 0; level < floorsToClimb; level++){
                    int beforeZIndex = this.methodProvider.myPlayer().getPosition().getZ();
                    stairs = this.methodProvider.getObjects().closest("Staircase");
                    interactionEvent = new InteractionEvent(stairs, "Climb-up");
                    interactionEvent.setOperateCamera(false);
                    this.methodProvider.execute(interactionEvent);
                    MethodProvider.sleep(1000);
                    
                    while(beforeZIndex == this.methodProvider.myPlayer().getPosition().getZ()){
                        interactionEvent = new InteractionEvent(stairs, "Climb-up");
                        interactionEvent.setOperateCamera(false);

                         // refactorizar
                        while(!this.isNearPosition(this.methodProvider.myPosition(), site)){
                            webWalkEvent = new WebWalkEvent(site);
                            webWalkEvent.setEnergyThreshold(energyThreshold);
                            webWalkEvent.setMoveCameraDuringWalking(false);
                            this.methodProvider.getSettings().setRunning(true);
                            this.methodProvider.execute(webWalkEvent);
                            MethodProvider.sleep(1000);

                            while(this.methodProvider.myPlayer().isMoving()){
                                MethodProvider.sleep(2000);
                            }
                        }

                        this.cameraManagement.normalize();

                        this.methodProvider.execute(interactionEvent);
                        MethodProvider.sleep(1000);
                        
                    }
                }
            }
            else if(floorsToGoDown != 0){
                for(int level = 0; level < floorsToGoDown; level++){
                    int beforeZIndex = this.methodProvider.myPlayer().getPosition().getZ();
                    stairs = this.methodProvider.getObjects().closest("Staircase");
                    interactionEvent = new InteractionEvent(stairs, "Climb-down");
                    interactionEvent.setOperateCamera(false);
                    this.methodProvider.execute(interactionEvent);
                    MethodProvider.sleep(1000);
                    
                    while(beforeZIndex == this.methodProvider.myPlayer().getPosition().getZ()){
                        interactionEvent = new InteractionEvent(stairs, "Climb-down");
                        interactionEvent.setOperateCamera(false);

                                                     // refactorizar
                        while(!this.isNearPosition(this.methodProvider.myPosition(), site)){
                            webWalkEvent = new WebWalkEvent(site);
                            webWalkEvent.setEnergyThreshold(energyThreshold);
                            webWalkEvent.setMoveCameraDuringWalking(false);
                            this.methodProvider.getSettings().setRunning(true);
                            this.methodProvider.execute(webWalkEvent);
                            MethodProvider.sleep(1000);

                            while(this.methodProvider.myPlayer().isMoving()){
                                MethodProvider.sleep(2000);
                            }
                        }

                        this.cameraManagement.normalize();

                        this.methodProvider.execute(interactionEvent);
                        MethodProvider.sleep(1000);
                        
                    }
                }
            }
        }
        
        return true;
    }
}
